
from helpers import list_helpers
import re #regular expression
import time
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy import ForeignKey, PrimaryKeyConstraint, ForeignKeyConstraint
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from random import choice
from string import letters

import sys


engine = create_engine('sqlite:///data/oodie.db', echo=True)
Base = declarative_base()

class Course(Base):

	__tablename__ = 'courses'
	id = Column(Integer, primary_key=True)
	date_str = Column(String)
	title = Column(String)
	credits = Column(Integer)
	grade = Column(String)
	student_id = Column(Integer, ForeignKey('students.id'))


	def __init(self, id=None, date_str="", title="", credits=0, grade=""):
		self.id = id
		self.date_str = date_str
		self.date = time.strptime(date_str, '%Y-%m')
		self.title = title
		self.credits = credits
		self.grade = grade


	@staticmethod
	def course_from_tuple(course_tuple):
		
		as_list = list(course_tuple) #convert the tuple to list

		as_list[2] = as_list[2].replace('"','') #remove " in course titles

		id = as_list[1].strip()

		date_str = as_list[0].strip()
		
		title = as_list[2].strip().decode('utf-8')
		credits = as_list[3].strip()
		grade = as_list[4].strip().decode('utf-8')

		return Course(id=id, date_str=date_str, title=title, credits=credits, grade=grade)



class Student(Base):

	__tablename__ = 'students'
	id = Column(Integer, primary_key=True)
	registration_year = Column(Integer)
	line = Column(String)
	courses = relationship("Course", order_by="Course.id", backref="student")

	#initialization function, takes a line as parameter and parses it
	def __init__(self, id=None, registration_year=0, line="", courses=[]):
		self.id = id
		self.registration_year = registration_year
		self.line = line.decode('utf-8')
		self.courses = courses



	@staticmethod
	def student_from_line(line):
		'''
			line format is follows:

			REGISTRATION_YEAR [course_year_and_month course_code "course name" credits final_grade ]*
		'''

		
		s_courses = []
		registration_year = None

		#split by line but ignore spaces inside double quotes
		data = re.split(''' (?=(?:[^'"]|'[^']*'|"[^"]*")*$)''', line.strip()) 
		
		registration_year = data[0]
		del data[0] #remove registration year
		

		#group the rest of the data as groups of 5
		courses = list_helpers.grouper(5, data)
		for course in courses:
			c = Course.course_from_tuple(course)
			s_courses.append(c)

		return Student(registration_year=registration_year, line=line, courses=s_courses)


# class StudentCourses(Base):

# 	__tablename__ = 'students_courses'

# 	student_id = Column(Integer, ForeignKey('students.id'))
# 	course_id = Column(Integer, ForeignKey('courses.id'))
# 	__table_args__ = (
# 		PrimaryKeyConstraint('student_id', 'course_id'),
# 		{},
# 	)

# 	student = relationship("Student", backref=backref('courses', order_by=id))
# 	course = relationship("Course", backref=backref('students', order_by=id))





# ALL_STUDENTS = [] # alist containing all the students
# ALL_COURSES = [] # a list containing all the courses
# STUDENT_COURSE = []
# Base.metadata.create_all(engine)
# Session = sessionmaker(bind=engine)
# s = Session()


# class Oodi:
# 	def __init__(self):
# 		pass
	
# 	@staticmethod
# 	def load(DATA_PATH):
# 		data = open(DATA_PATH, 'r')
# 		for line in data:
# 			s = Student.student_from_line(line)
# 			ALL_STUDENTS.append(s)

# 			for c in s.courses:
# 				ALL_COURSES.append(c)


# 	@staticmethod
# 	def save_to_db():		
# 		# print "STUDENTS LEN:" + str(len(ALL_STUDENTS))
# 		# s.add_all(ALL_COURSES + ALL_STUDENTS)
# 		# s.commit()
# 		pass