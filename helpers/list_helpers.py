from itertools import izip_longest

def grouper(n, iterable, fillvalue=None):
	"grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx" #http://stackoverflow.com/a/4998461
	args = [iter(iterable)] * n
	return izip_longest(fillvalue=fillvalue, *args)