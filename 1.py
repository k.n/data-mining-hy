from classes.oodi import Student

DATA_PATH = 'data/data.txt' #where the data is stored
DATA_MOD_PATH = 'data/mod_data.txt' #in case if we changed something, store it to this file

ALL_STUDENTS = []
ALL_COURSES = {}


data = open(DATA_PATH, 'r')
for line in data:
	s = Student.student_from_line(line)
	ALL_STUDENTS.append(s)

	for course in s.courses:
		if course.id not in ALL_COURSES:
			ALL_COURSES[course.id] = []
		ALL_COURSES[course.id].append(course)
	

'''
"Introduction to programming" -- "Ohjelmoinnin perusteet"
"Advanced programming" -- "Ohjelmoinnin jatkokurssi", also "Java-ohjelmointi"
"Data structures" -- "Tietorakenteet ja algoritmit"

Course grades

Hyl. -> fail
Eisa -> fail
Luop -> fail

Hyv. -> pass
HT -> pass
TT -> pass

NCLA, CLA, ... -> pass
'''

PASSED_GRADES = ['Hyv', 'HT', 'TT', '1', '2', '3', '4', '5', 'NCLA', 'CLA']
FAIL_GRADES = ['Hyl', 'Eisa', 'Luop']

def times_course_taken(name):
	for course_id in ALL_COURSES.keys():
		if name in ALL_COURSES[course_id][0].title:
			return ALL_COURSES[course_id]

def count_students_taking_course(courses = []):
	count = 0
	for student in ALL_STUDENTS: #for all the students
		local_count = 0 #to count if all courses passed are taken
		l_courses = courses
		for course in student.courses: #the courses the student took	
			for given_course in l_courses: #for courses passed
				if given_course in course.title:
					local_count += 1

					l_courses.remove(given_course) #remove it to prevent duplicates
					break

		if local_count == len(courses):
			count += 1

	return count

def count_students_passed_course(courses=[], grades=PASSED_GRADES):
	count = 0
	for student in ALL_STUDENTS: #for all the students
		local_count = 0 #to count if all courses passed are taken
		l_courses = courses
		for course in student.courses: #the courses the student took	
			for given_course in l_courses: #for courses passed
				if given_course in course.title and course.grade in grades:
					local_count += 1

					l_courses.remove(given_course) #remove it to prevent duplicates
					break

		if local_count == len(courses):
			count += 1

	return count

print len(ALL_COURSES) #6160
print len(ALL_STUDENTS) #2876


def support_courses(A=[], B=[]):
	A = list(set(A))
	B = list(set(B))
	A_uin_B = list(set(A) | set(B))

	return float(count_students_taking_course(A_uin_B))/len(ALL_COURSES)



def confidence_courses(A=[], B=[]):
	return float(support_courses(A, B))/support_courses(A)


#ASSIGNMENT 1
#get unique courses by code
# for id in ALL_COURSES.keys():
# 	print id

print len(times_course_taken('Ohjelmoinnin perusteet')) #1542
print count_students_taking_course(['Ohjelmoinnin perusteet']) #1269
print count_students_passed_course(['Ohjelmoinnin perusteet']) #2873
print count_students_passed_course(['Ohjelmoinnin jatkokurssi', 'Java-ohjelmointi']) #2779
print count_students_passed_course(['Tietorakenteet ja algoritmit'], ['4', '5']) #2787
print count_students_passed_course(['Ohjelmoinnin perusteet', 'Ohjelmoinnin jatkokurssi', 'Java-ohjelmointi']) #2780
print count_students_passed_course(['Ohjelmoinnin perusteet', 'Ohjelmoinnin jatkokurssi', 'Tietorakenteet ja algoritmit', 'Java-ohjelmointi']) #2780
print count_students_passed_course(['Ohjelmoinnin perusteet', 'Ohjelmoinnin jatkokurssi', 'Tietorakenteet ja algoritmit', 'Java-ohjelmointi'], ['4','5']) #2767



print float(float(count_students_taking_course(['Ohjelmoinnin perusteet', 'Ohjelmoinnin jatkokurssi']))/len(ALL_COURSES))*100
print count_students_taking_course(['Ohjelmoinnin perusteet', 'Ohjelmoinnin jatkokurssi']) 
print float(confidence_courses(['Ohjelmoinnin perusteet'], ['Ohjelmoinnin jatkokurssi'])) * 100